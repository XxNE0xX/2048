﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class Graph
    {
        private int gridSize = 4;

        private int maxCell = 0;

        private int score = 0;
        private List<Node> mainNodesList = new List<Node>();
        private List<Node> emptyNodesList = new List<Node>();

        private const bool success = true;

        public Graph(int gridSize)
        {
            this.gridSize = gridSize;
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    Node node = new Node(i * gridSize + j);
                    mainNodesList.Add(node);
                    emptyNodesList.Add(node);
                }
            }
        }

        public Graph()
        {
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    Node node = new Node(i * gridSize + j);
                    mainNodesList.Add(node);
                    emptyNodesList.Add(node);
                }
            }
        }

        public void LoadGraphByArray(int[,] values)
        {
            emptyNodesList = new List<Node>();
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    mainNodesList[i * gridSize + j].SetValue(values[i, j]);
                    if (values[i, j] == 1)
                    {
                        emptyNodesList.Add(mainNodesList[i * gridSize + j]);
                    }
                }
            }
        }

        public void LoadGraphByList(List<int> values)
        {
            emptyNodesList = new List<Node>();
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    mainNodesList[i * gridSize + j].SetValue(values[i * gridSize + j]);
                    if (values[i * gridSize + j] == 1)
                    {
                        emptyNodesList.Add(mainNodesList[i * gridSize + j]);
                    }
                }
            }
        }

        public int GetValueOfCellByIndex(int index)
        {
            return mainNodesList[index].GetValue();
        }

        public bool IsAnyMoveLeft()
        {
            // Look for the same values in columns
            for (int i = 0; i < gridSize; i++)
            {
                int seenValue = -1;
                for (int j = 0; j < gridSize; j++)
                {
                    if (seenValue != -1)
                    {
                        if (seenValue == mainNodesList[i * gridSize + j].GetValue()/* && seenValue != 0*/)
                        {
                            return success;
                        }
                    }
                    seenValue = mainNodesList[i * gridSize + j].GetValue();
                }
            }
            // Look for the same values in rows
            for (int j = 0; j < gridSize; j++)
            {
                int seenValue = -1;
                for (int i = 0; i < gridSize; i++)
                {
                    if (seenValue != -1)
                    {
                        if (seenValue == mainNodesList[i * gridSize + j].GetValue()/* && seenValue != 0*/)
                        {
                            return success;
                        }
                    }
                    // Returns true if there's an empty cell
                    seenValue = mainNodesList[i * gridSize + j].GetValue();
                }
            }

            if (emptyNodesList.Count != 0)
            {
                return success;
            }

            return !success;
        }

        public bool MergeCells(int mergedCellIndex, int mergingCellIndex)
        {
            bool mergeHappend = false;
            // We don't want double merge in one turn
            if (mainNodesList[mergedCellIndex].IsMerged() == false)
            {
                // Here the program checks if these two cells have same values, and also we don't want to merge empty cells so we check the value for != 1
                if (mainNodesList[mergedCellIndex].GetValue() == mainNodesList[mergingCellIndex].GetValue() && mainNodesList[mergedCellIndex].GetValue() != 1)
                {
                    score += mainNodesList[mergedCellIndex].GetValue() * 2;
                    if (mainNodesList[mergedCellIndex].GetValue() * 2 > maxCell)
                    {
                        maxCell = mainNodesList[mergedCellIndex].GetValue() * 2;
                    }
                    FillNode(mainNodesList[mergedCellIndex], mainNodesList[mergedCellIndex].GetValue() * 2); // Doubling the merged cell value
                    mainNodesList[mergedCellIndex].SetMergedStatus(true);   // We don't want double merge so we mark the flag
                    EmptyNode(mainNodesList[mergingCellIndex]);    // Reseting the merging cell value into default

                    mergeHappend = true;
                }
            }
            return mergeHappend;
        }

        public void SpawnRandomNewCell()
        {
            int randomIndex = Random.Range(0, emptyNodesList.Count);
            Node chosenNode = emptyNodesList[randomIndex];
            FillNode(chosenNode, 2 * Random.Range(1, 3));
        }

        public bool MoveUp()
        {
            bool onePieceMoved = false;
            for (int j = 0; j < gridSize; j++)
            {
                for (int i = 0; i < gridSize; i++)
                {
                    int newRowIndex = i;
                    for (int k = i; k >= 0; k--)
                    {
                        if (k + 1 < gridSize)
                        {
                            // Moving the cells to the most-left empty position and not moving empty cells
                            if (mainNodesList[k * gridSize + j].GetValue() == 1 && mainNodesList[(k + 1) * gridSize + j].GetValue() != 1)
                            {
                                FillNode(mainNodesList[k * gridSize + j], mainNodesList[(k + 1) * gridSize + j].GetValue());
                                EmptyNode(mainNodesList[(k + 1) * gridSize + j]);
                                newRowIndex = k;
                                onePieceMoved = true;
                            }
                        }
                    }
                    if (newRowIndex > 0)
                    {
                        bool mergeHappend = MergeCells((newRowIndex - 1) * gridSize + j, newRowIndex* gridSize + j);
                        if (onePieceMoved == false)
                        {
                            onePieceMoved = mergeHappend;
                        }
                    }
                }
            }
            // Reseting every node merge status
            ResetAllMergeStatuses();
            return onePieceMoved;
        }

        public bool MoveDown()
        {
            bool onePieceMoved = false;
            for (int j = 0; j < gridSize; j++)
            {
                for (int i = gridSize - 1; i >= 0; i--)
                {
                    int newRowIndex = i;
                    for (int k = i; k < gridSize; k++)
                    {
                        if (k + 1 < gridSize)
                        {
                            // Moving the cells to the most-right empty position and not moving empty cells
                            if (mainNodesList[(k + 1) * gridSize + j].GetValue() == 1 && mainNodesList[k * gridSize + j].GetValue() != 1)
                            {
                                FillNode(mainNodesList[(k + 1) * gridSize + j], mainNodesList[k * gridSize + j].GetValue());
                                EmptyNode(mainNodesList[k * gridSize + j]);
                                newRowIndex = k + 1;
                                onePieceMoved = true;
                            }
                        }
                    }
                    if (newRowIndex < gridSize - 1)
                    {
                        bool mergeHappend = MergeCells((newRowIndex + 1) * gridSize + j, newRowIndex * gridSize + j);
                        if (onePieceMoved == false)
                        {
                            onePieceMoved = mergeHappend;
                        }
                    }
                }
            }
            // Reseting every node merge status
            ResetAllMergeStatuses();
            return onePieceMoved;
        }

        public bool MoveLeft()
        {
            bool onePieceMoved = false;
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    int newColumnIndex = j;
                    for (int k = j; k >= 0; k--)
                    {
                        if (k + 1 < gridSize)
                        {
                            // Moving the cells to the most-left empty position and not moving empty cells
                            if (mainNodesList[i * gridSize + k].GetValue() == 1 && mainNodesList[i * gridSize + (k + 1)].GetValue() != 1)
                            {
                                FillNode(mainNodesList[i * gridSize + k], mainNodesList[i * gridSize + (k + 1)].GetValue());
                                EmptyNode(mainNodesList[i * gridSize + (k + 1)]);
                                newColumnIndex = k;
                                onePieceMoved = true;
                            }
                        }
                    }
                    if (newColumnIndex > 0)
                    {
                        bool mergeHappend = MergeCells(i * gridSize + (newColumnIndex - 1), i * gridSize + newColumnIndex);
                        if (onePieceMoved == false)
                        {
                            onePieceMoved = mergeHappend;
                        }
                    }
                }
            }
            // Reseting every node merge status
            ResetAllMergeStatuses();
            return onePieceMoved;
        }

        public bool MoveRight()
        {
            bool onePieceMoved = false;
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = gridSize - 1; j >= 0; j--)
                {
                    int newColumnIndex = j;
                    for (int k = j; k < gridSize; k++)
                    {
                        if (k + 1 < gridSize)
                        {
                            // Moving the cells to the most-right empty position and not moving empty cells
                            if (mainNodesList[i * gridSize + (k + 1)].GetValue() == 1 && mainNodesList[i * gridSize + k].GetValue() != 1)
                            { 
                                FillNode(mainNodesList[i * gridSize + (k + 1)], mainNodesList[i * gridSize + k].GetValue());
                                EmptyNode(mainNodesList[i * gridSize + k]);
                                newColumnIndex = k + 1;
                                onePieceMoved = true;
                            }
                        }
                    }
                    if (newColumnIndex < gridSize - 1)
                    {
                        bool mergeHappend = MergeCells(i * gridSize + (newColumnIndex + 1), i * gridSize + newColumnIndex);
                        if (onePieceMoved == false)
                        {
                            onePieceMoved = mergeHappend;
                        }
                    }
                }
            }
            // Reseting every node merge status
            ResetAllMergeStatuses();
            return onePieceMoved;
        }

        private void EmptyNode(Node node)
        {
            node.SetValue(1);
            emptyNodesList.Add(node);
        }
        private void FillNode(Node node, int newValue)
        {
            node.SetValue(newValue);
            if (emptyNodesList.Contains(node))
            {
                emptyNodesList.Remove(node);
            }
        }

        private void ResetAllMergeStatuses()
        {
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    mainNodesList[i * gridSize + j].SetMergedStatus(false);
                }
            }
        }

        override public string ToString()
        {
            string output = "";
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    output += GetValueOfCellByIndex(i * gridSize + j);
                    if (j != gridSize - 1)
                    {
                        output += " ";
                    }
                }
                if (i != gridSize - 1)
                {
                    output += "\n";
                }
            }
            return output;
        }

        public int GetScore()
        {
            return score;
        }

        public int GetMaxCell()
        {
            return maxCell;
        }

        // Only use this method when loading the graph!
        public void LoadScore(int score)
        {
            this.score = score;
        }
    }
}
