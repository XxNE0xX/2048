﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using TMPro;
using UnityEngine.UI;

public class Board : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    [Tooltip("The maximum exponent of 2 which after that, the cells become black")][SerializeField] private int maxPower = 16;
    private GameObject emptyCellPrefab;


    private int gridSize;

    private bool isValueCorrected = false;

    private List<GameObject> cells = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        emptyCellPrefab = Resources.Load<GameObject>("Prefabs/EmptyCell");
        gridSize = gameManager.GetGridSize();
        InstantiateCells();
    }

    // Update is called once per frame
    void Update()
    {
        if (isValueCorrected == false)
        {
            if (gameManager.IsGraphInitiated() == true)
            {
                CorrectValuesAndColor();
                isValueCorrected = true;
            }
        }
    }

    public void ValuesHaveChanged()
    {
        isValueCorrected = false;
    }

    private void InstantiateCells()
    {
        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {
                GameObject cell = Instantiate(emptyCellPrefab) as GameObject;
                cell.transform.SetParent(this.transform, false);
                cells.Add(cell);
            }
        }
    }

    private void CorrectValuesAndColor()
    {
        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {
                int value = gameManager.GetValueOfCellByIndex(i * gridSize + j);
                if (value != 1)
                {
                    string valueByString = value.ToString();
                    // Correct Showing Value
                    cells[i * gridSize + j].GetComponentInChildren<TextMeshProUGUI>().SetText(valueByString);
                }
                else
                {
                    cells[i * gridSize + j].GetComponentInChildren<TextMeshProUGUI>().SetText("");
                }
                // Correct Color
                float thePower = Mathf.Log(value, 2);
                float newColorValues = 255f;
                if (thePower <= maxPower && thePower != 0)
                {
                    newColorValues = 255f - (255f / (float)maxPower) * thePower;
                }
                else if (thePower <= maxPower && thePower == 0)
                {
                    newColorValues = 255f;
                }
                else
                {
                    newColorValues = 0f;
                }
                
                if (newColorValues == 255)
                {
                	cells[i * gridSize + j].GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
                }
                else
                {
                cells[i * gridSize + j].GetComponent<Image>().color = new Color(248f / 255f, 212f / 255f, newColorValues / 255f, 1f);
            	}
            }
        }
    }

}
