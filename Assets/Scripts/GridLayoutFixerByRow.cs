﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridLayoutFixerByRow : MonoBehaviour
{
    [Tooltip("How many columns this panel is going to have")] [SerializeField] private int howManyColumn = 4;
    [Tooltip("How many rows this panel is going to have")][SerializeField] private int howManyRow = 2;
    [Tooltip("How many of the rows are not being used")] [SerializeField] private int howManyRowNotUsed = 2;

    private float oldWidth = 0;
    private float oldHeight = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float panelWidth = this.GetComponent<RectTransform>().rect.width;
        float panelHeight = this.GetComponent<RectTransform>().rect.height;
        if (oldWidth != panelWidth)
        {
            this.GetComponent<GridLayoutGroup>().cellSize = new Vector2(panelWidth, panelHeight / howManyRow);
            oldWidth = panelWidth;
            oldHeight = panelHeight;
        }
    }
}
