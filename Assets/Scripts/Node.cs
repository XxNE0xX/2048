﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class Node
    {
        private int index;
        private int value = 1;  // keeps the value of the cell, initially it's one
        private bool isMerged = false;

        public Node(int value, int index)
        {
            this.index = index;
            this.value = value;
        }

        public Node(int index)
        {
            this.index = index;
            value = 1;
        }

        public int GetIndex()
        {
            return index;
        }

        public int GetValue()
        {
            return value;
        }
        public void SetValue(int value)
        {
            this.value = value;
        }
        public bool IsMerged()
        {
            return isMerged;
        }
        public void SetMergedStatus(bool mergedStatus)
        {
            isMerged = mergedStatus;
        }

    }
}
