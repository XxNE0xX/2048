﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class FactPanelUpdater : MonoBehaviour
{

    [Tooltip("Put the hexadecimal value of the desired color to apply to all boxes within this panel")][SerializeField] private int color;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //UpdateColor();
    }


    private void UpdateColor()
    {
        throw new NotImplementedException();
    }

    public void UpdateValue(int newValue)
    {
        // Second child is the value keeper
        this.transform.GetChild(1).GetComponent<TextMeshProUGUI>().SetText(newValue.ToString());
    }
}
