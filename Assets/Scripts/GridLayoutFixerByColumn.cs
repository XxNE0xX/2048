﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridLayoutFixerByColumn : MonoBehaviour
{
    [Tooltip("How many columns this panel is going to have")] [SerializeField] private int howManyColumn = 4;
    [Tooltip("How many of the columns are not being used")] [SerializeField] private int howManyColumnNotUsed = 2;
    [Tooltip("How many rows this panel is going to have")] [SerializeField] private int howManyRow = 4;

    private float oldWidth = 0;
    private float oldHeight = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float panelWidth = this.GetComponent<RectTransform>().rect.width;
        float panelHeight = this.GetComponent<RectTransform>().rect.height;
        if (oldHeight != panelHeight)
        {
            float newSpacing = ((panelWidth / howManyColumn) * howManyColumnNotUsed) / (howManyColumn - howManyColumnNotUsed);
            Vector2 newSpacingVector = new Vector2(newSpacing, newSpacing);
            this.GetComponent<GridLayoutGroup>().cellSize = new Vector2(panelWidth / howManyColumn, panelHeight / howManyRow);
            this.GetComponent<GridLayoutGroup>().spacing = newSpacingVector;

            oldWidth = panelWidth;
            oldHeight = panelHeight;
        }
    }
}
