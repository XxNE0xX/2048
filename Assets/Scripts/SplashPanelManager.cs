﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashPanelManager : MonoBehaviour
{
    [SerializeField] private int howManyRows = 2;

    private float oldWidth = 0;
    private float eachCellHeight = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckAndUpdateSize();
    }

    private void CheckAndUpdateSize()
    {
        float width = this.GetComponent<RectTransform>().rect.width;
        if (oldWidth != width)
        {
            // Change its own transform
            eachCellHeight = this.GetComponent<RectTransform>().rect.height / howManyRows;
            this.GetComponent<GridLayoutGroup>().cellSize = new Vector2(width, eachCellHeight);
            oldWidth = width;
        }
    }

    public float GetWidth()
    {
        return oldWidth;
    }
    public float GetHeight()
    {
        return eachCellHeight;
    }
}
