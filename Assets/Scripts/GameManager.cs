﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using System;
using System.IO;

public class GameManager : MonoBehaviour
{
    [SerializeField] private int gridSize = 4;
    [SerializeField] private CellResizer cellResizer;
    [SerializeField] private Board board;
    [SerializeField] private int WinningValue = 2048;

    [SerializeField] private GameObject winSplashPanel;
    [SerializeField] private GameObject gameOverSplashPanel;

    private bool isSplashScreenShowing = false;
    private bool isContinued = false;

    private string savedDataPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/2048data";
    private string savedBoardDataFileName = "/board.gdata";
    private string savedScoreDataFileName = "/score.gdata";

    private int score = 0;   // Score achieved so far
    private int bestScore;  // Read from file, best score of the player
    private int maxCell;

    private Graph graph;
    private bool isGraphInitiated = false;

    // Start is called before the first frame update
    void Start()
    {
        graph = new Graph(gridSize);
        // graph.LoadGraphByArray(new int[,]{
        //     { 2, 4, 8, 16},
        //     { 16, 8, 64, 32},
        //     { 4, 2, 1024, 128},
        //     { 4, 16, 256, 8}
        // });
        isGraphInitiated = true;
        graph.SpawnRandomNewCell();

        LoadBestScore();

        cellResizer.setGridSize(gridSize);
    }

    // Update is called once per frame
    void Update()
    {
        score = graph.GetScore();
        if (score > bestScore)
        {
            bestScore = score;
        }
        maxCell = graph.GetMaxCell();
        if (isContinued == false && maxCell >= WinningValue)
        {
            Win();
        }

        if (isSplashScreenShowing == false)
        {
            CheckKeyPress();

            if (graph.IsAnyMoveLeft() == false)
            {
                GameOver();
            }
        }
    }

    private void CheckKeyPress()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            MoveUp();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MoveDown();
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MoveLeft();
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MoveRight();
        }
        else if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) &&
            (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) &&
            Input.GetKeyDown(KeyCode.S))
        {
            SaveGame();
        }
        else if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) &&
            (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) &&
            Input.GetKeyDown(KeyCode.L))
        {
            LoadGame();
        }
        else if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) &&
            (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) &&
            Input.GetKeyDown(KeyCode.N))
        {
            NewGame(null);
        }
    }

    private void GameOver()
    {
        SaveBestScore();
        gameOverSplashPanel.SetActive(true);
        isSplashScreenShowing = true;
    }

    private void Win()
    {
        winSplashPanel.SetActive(true);
        isSplashScreenShowing = true;
    }

    public void NewGame(GameObject splashScreen)
    {
        if (splashScreen != null)
        {
            splashScreen.SetActive(false);
            isSplashScreenShowing = false;
        }
        graph = new Graph(gridSize);
        graph.SpawnRandomNewCell();

        board.ValuesHaveChanged();
        score = 0;
        LoadBestScore();
        
    }

    public void ContinueGame(GameObject splashScreen)
    {
        splashScreen.SetActive(false);
        isSplashScreenShowing = false;
        isContinued = true;
    }

    private void MoveUp()
    {
        if (graph.MoveUp() == true)
        {
            board.ValuesHaveChanged();
            graph.SpawnRandomNewCell();
        }
    }
    private void MoveDown()
    {
        if (graph.MoveDown() == true)
        {
            board.ValuesHaveChanged();
            graph.SpawnRandomNewCell();
        }
    }
    private void MoveLeft()
    {
        if (graph.MoveLeft() == true)
        {
            board.ValuesHaveChanged();
            graph.SpawnRandomNewCell();
        }

    }
    private void MoveRight()
    {
        if (graph.MoveRight() == true)
        {
            board.ValuesHaveChanged();
            graph.SpawnRandomNewCell();
        }
    }

    private void SaveGame()
    {
        string output = graph.ToString() + "\n" + score;
        Directory.CreateDirectory(savedDataPath);
        File.WriteAllText(savedDataPath + savedBoardDataFileName, output);
        SaveBestScore();
        Debug.Log("File Saved!");
    }

    private void LoadGame()
    {
        if (File.Exists(savedDataPath + savedBoardDataFileName))
        {
            List<int> values = new List<int>();
            string inputString = "";
            StreamReader streamReader = new StreamReader(savedDataPath + savedBoardDataFileName);
            while (streamReader.EndOfStream == false)
            {
                inputString = streamReader.ReadLine();
                string[] numbers = inputString.Split(' ');
                if (numbers.Length > 1)
                {
                    foreach (string number in numbers)
                    {
                        values.Add(Int32.Parse(number));
                    }
                }
                else
                {
                    score = Int32.Parse(numbers[0]);
                    graph.LoadScore(score);
                }
            }
            streamReader.Close();

            graph.LoadGraphByList(values);
            board.ValuesHaveChanged();
            
        }
        else
        {
            Debug.Log("The Graph File Doesn't Exist!");
        }

        LoadBestScore();

    }

    private void SaveBestScore()
    {
        File.WriteAllText(savedDataPath + savedScoreDataFileName, bestScore.ToString());
    }

    private void LoadBestScore()
    {
        if (File.Exists(savedDataPath + savedScoreDataFileName))
        {
            StreamReader streamReader = new StreamReader(savedDataPath + savedScoreDataFileName);
            bestScore = Int32.Parse(streamReader.ReadLine());
            streamReader.Close();
        }
        else
        {
            Debug.Log("The Score File Doesn't Exist!");
            bestScore = 0;
        }
    }

    public int GetGridSize()
    {
        return gridSize;
    }

    public int GetScore()
    {
        return score;
    }

    public int GetBestScore()
    {
        return bestScore;
    }

    public bool IsGraphInitiated()
    {
        return isGraphInitiated;
    }

    public int GetValueOfCellByIndex(int index)
    {
        return graph.GetValueOfCellByIndex(index);
    }
}
