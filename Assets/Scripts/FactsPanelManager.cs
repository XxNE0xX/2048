﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FactsPanelManager : MonoBehaviour
{
    [SerializeField] private GameObject boardPanelBackground;
    [SerializeField] private int howManyRows = 4;
    [SerializeField] private GameManager gameManager;
    private float oldWidth = 0;
    private float eachCellHeight = 0;

    private int score = 0;
    private int bestScore;
    // Start is called before the first frame update
    void Start()
    {
        bestScore = gameManager.GetBestScore();
        UpdateBestScore();
        UpdateScore();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateShowingValues();
        CheckAndUpdateSize();
        
    }

    private void UpdateShowingValues()
    {
        int newScore = gameManager.GetScore();
        if (newScore != score)
        {
            score = newScore;
            UpdateScore();
        }
        int newBestScore = gameManager.GetBestScore();
        if (newBestScore != bestScore)
        {
            bestScore = newBestScore;
            UpdateBestScore();
        }
    }

    private void CheckAndUpdateSize()
    {
        float boardPanelActualWidth = boardPanelBackground.GetComponent<RectTransform>().rect.width * boardPanelBackground.transform.localScale.x;
        float width = (Screen.width - boardPanelActualWidth) / 2;
        if (oldWidth != width)
        {
            // Change its own transform
            this.GetComponent<RectTransform>().sizeDelta = new Vector2(width, 0);
            eachCellHeight = this.GetComponent<RectTransform>().rect.height / howManyRows;
            this.GetComponent<GridLayoutGroup>().cellSize = new Vector2(this.GetComponent<RectTransform>().rect.width, eachCellHeight);
            oldWidth = width;
        }
    }

    private void UpdateScore()
    {
        // First child keeps the score
        this.transform.GetChild(0).GetComponent<FactPanelUpdater>().UpdateValue(score);
    }

    private void UpdateBestScore()
    {
        // First child keeps the score
        this.transform.GetChild(1).GetComponent<FactPanelUpdater>().UpdateValue(bestScore);
    }
}
