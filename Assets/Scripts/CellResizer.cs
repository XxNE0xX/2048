﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellResizer : MonoBehaviour
{
    private float oldWidth = 0;
    private int gridSize;
    private bool isGridSizeSet = false;
    [SerializeField] private GameObject boardPanelBackground;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isGridSizeSet)
        {
            CheckForChangeAndCorrectSize();
        }
    }

    private void CheckForChangeAndCorrectSize()
    {
        this.GetComponent<GridLayoutGroup>().constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        this.GetComponent<GridLayoutGroup>().constraintCount = gridSize;

        float width = this.GetComponent<RectTransform>().rect.width * this.transform.localScale.x;
        if (width != oldWidth)
        {
            oldWidth = width;
            // we need to check the real width of these objects that's why we scale them
            float outerLayerWidth = boardPanelBackground.GetComponent<RectTransform>().rect.width * boardPanelBackground.transform.localScale.x;
            float differenceWithOuterLayer = outerLayerWidth - width;
            // the spaces between cell = gridSize - 1
            float newSize = (width - (differenceWithOuterLayer / 2 * (gridSize - 1))) / gridSize;
            // each space is equal to half of the difference
            float newSpacing = differenceWithOuterLayer / 2;

            // grid layout properties don't need to be scaled
            float xScale = this.transform.localScale.x;
            float yScale = this.transform.localScale.y;
            Vector2 newSizeVector = new Vector2(newSize / xScale, newSize / yScale);
            Vector2 newSpacingVector = new Vector2(newSpacing / xScale, newSpacing / yScale);

            this.GetComponent<GridLayoutGroup>().cellSize = newSizeVector;
            this.GetComponent<GridLayoutGroup>().spacing = newSpacingVector;
        }
    }

    public void setGridSize(int gridSize)
    {
        this.gridSize = gridSize;
        isGridSizeSet = true;
    }
}
